package com.user.associateCreateCustomer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.user.associateCreateCustomer.entity.Account;
import com.user.associateCreateCustomer.entity.Customer;
import com.user.associateCreateCustomer.entity.SignInRequest;
import com.user.associateCreateCustomer.repository.CustomerRepository;

@Component
public class CustomerDigitPinService {

			
		private CustomerRepository repository;
		
		@Autowired
		public CustomerDigitPinService(CustomerRepository repository) {
			this.repository = repository;
		}
		
		public void getDigitaPin(String pin){
			
		}
		
		public void newCustomerDigitaPin(String customerId){
			
		}
		
		public void forgotDigitaPin(String customerId){
			
		}
		
		public void resetDigitaPin(String customerId){
			
		}
		
		public boolean loginCustomer(SignInRequest signInForm) {
			boolean loginSuccess=false;
			return loginSuccess;
		}
		
		public Customer getCustomerDetails(long customerNumber) {
			return repository.findById(customerNumber).get();
		}
		
		public void associateRegistration(Customer request){
			Account account= new Account();
			account.setType("Pre-paid Service");
			account.setContact(request.getPhoneNum());
			account.setEmail(request.getEmailAddress());
			
			Customer customer=new Customer();
			customer.setAddress1(request.getAddress1());
			customer.setAddress2(request.getAddress2());
			customer.setAddressId(request.getAddressId());
			customer.setCity(request.getCity());
			customer.setEmailAddress(request.getEmailAddress());
			customer.setExistingCreditcard(false);
			customer.setFirstName(request.getFirstName());
			customer.setInstallationType(request.getInstallationType());
			customer.setLastName(request.getLastName());
			customer.setMigratePostpaid(false);
			customer.setOrderd(request.getOrderd());
			customer.setPhoneNum(request.getPhoneNum());
			customer.setZip(request.getZip());
			customer.setState(request.getState());
			customer.setPin(request.getPin());
			customer.setPhoneVerified(false);
			
			account.setCustomer(customer);

			repository.save(customer);
		}
		
	
}
