package com.user.associateCreateCustomer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.user.associateCreateCustomer.entity.Customer;
import com.user.associateCreateCustomer.entity.SignInRequest;
import com.user.associateCreateCustomer.service.CustomerDigitPinService;

@RestController
@RequestMapping("/api/v1/pin")
public class CustomerDigitPinController {

		
		private CustomerDigitPinService customerDigitPinService;

		@Autowired
		public CustomerDigitPinController(CustomerDigitPinService customerDigitPinService) {
			this.customerDigitPinService = customerDigitPinService;
		}
		
		@RequestMapping(value = "/login", method = RequestMethod.POST)
		public String loginAssociate(@RequestBody SignInRequest signInRequest) throws Exception {
			customerDigitPinService.loginCustomer(signInRequest);
			return "Login Successful";
		}

		@RequestMapping(value = "/register", method = RequestMethod.POST)
		public String register(@RequestBody Customer customer) throws Exception {
			customerDigitPinService.associateRegistration(customer);
			return "Registration Successful";
		}

		@RequestMapping(value = "/get/{customerNumber}", method = RequestMethod.GET)
		public Customer get(@PathVariable("customerNumber") long customerNumber)  {
			return customerDigitPinService.getCustomerDetails(customerNumber);
		}
	
	
}
