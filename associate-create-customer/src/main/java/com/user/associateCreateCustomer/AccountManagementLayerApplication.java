package com.user.associateCreateCustomer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccountManagementLayerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountManagementLayerApplication.class, args);
	}
}
