package com.user.associateCreateCustomer.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.user.associateCreateCustomer.entity.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long >{

}
